package com.devcamp.arrayfilterinputrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArrayFilterInputRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArrayFilterInputRestApiApplication.class, args);
	}

}
