package com.devcamp.arrayfilterinputrestapi.controller;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.arrayfilterinputrestapi.service.ArrayListService;

@RestController
@CrossOrigin
public class controllerArrayList {
    @Autowired
    private ArrayListService arrayListService;

    @GetMapping("/array-int-request-query")
    public ArrayList<Integer> getArrayIntRequestQuery(@RequestParam(name="pos", required =  true) int paramPos){
        return arrayListService.getArrayFilterPos(paramPos);    

    }

    @GetMapping("/array-int-request-query/{index}")
    public int getArrayIndex(@PathVariable(name="index", required =  true) int paramPos){
        return arrayListService.getArrayIndex(paramPos)  ; 

    }
    
}
