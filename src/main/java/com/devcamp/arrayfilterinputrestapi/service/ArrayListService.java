package com.devcamp.arrayfilterinputrestapi.service;

import java.util.ArrayList;
import org.springframework.stereotype.Service;

@Service
public class ArrayListService {
    private int[] rainbows = { 1, 23, 32, 43, 54, 65, 86, 10, 15, 16, 18 };

    // phương thức trả về array các phần tử có giá trị lớn hơn (pos)
    public ArrayList<Integer> getArrayFilterPos(int pos) {
        ArrayList<Integer> list = new ArrayList<Integer>();

        for (int rainbowElement : rainbows) {
            if (rainbowElement > pos) {
                list.add(Integer.valueOf(rainbowElement));
            }
        }
        return list;
    }

    // đầu vào là int index dạng Request param.
    // Dựa vào int index trả ra int số có vị trí thứ index.
    public int getArrayIndex(int index) {
        return rainbows[index];
    }

}
